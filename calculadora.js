// Inicialização das variáveis de cálculo
let somaNotas = 0;
let n = 0;

// Seleção dos Elementos
let btnAdd = document.querySelector(".btn-add");
let btnCalc = document.querySelector(".btn-calc");
let inputNota = document.querySelector("#nota");
let outputNota = document.querySelector("#notas-adicionadas");

// Funcionalidade dos Botões
btnAdd.addEventListener("click", () => {
    let nota = inputNota.value;
    if (nota == "") {
        alert("Por favor, insira uma nota.");
        return;
    }
    nota = Number(inputNota.value);
    if (nota != nota) {
        alert("A nota digitada é inválida. Por favor, insira uma nota válida.")
        return;
    }
    if ((nota < 0) || (nota > 10)) {
        alert("A nota digitada é inválida. Por favor, insira uma nota válida");
        return;
    }
    updateNota(nota);
    addLog(nota);
});

btnCalc.addEventListener("click", () => {
    let media = somaNotas / n;
    let mediaRounded = Math.round((media + Number.EPSILON) * 100) / 100;
    let outputMedia = document.querySelector("p");
    outputMedia.innerText = `A média é: ${mediaRounded}`;
})

// Funções de Apoio
function updateNota(num) {
    somaNotas += num;
    n++;
};

function addLog(num) {
    let novaMsg = `A nota ${n} foi ${num}`;
    outputNota.value += novaMsg + '\n';
}